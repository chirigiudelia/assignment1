
const FIRST_NAME = "Delia";
const LAST_NAME = "Chirigiu";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value <Number.MIN_SAFE_INTEGER || value > Number.MAX_SAFE_INTEGER)
        return NaN;
        
    return parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

